<!DOCTYPE html>
<html lang="fr">
    <head>
    	<!--
    	Développement : pancard.fr
    	Copyright - Tous droits réservés - à partir de 2013
    	-->
		<meta charset="utf-8" />
        <meta name="Author" CONTENT="num1nfo" />
        <meta name="Robots" CONTENT="all" />
        <meta name="Keywords" CONTENT="assistance,informatique,dombasle,conseils,initiation,formation,cours,achat,vente,ordinateur,auto entrepreneur,internet,télécom,numérisation,tarif,Dombasle-sur-Meurthe,54,54110" />
        <meta name="Description" content="Num1nfo intervient chez vous et vous assistera dans l’Initiation et l'Installation Informatique, le conseil en achats num&eacute;riques et abonnements t&eacute;l&eacute;com" />
        <link rel="shortcut icon" href="images/1.ico" /> 
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/formulaire.css" />
        <link rel="stylesheet" href="css/popup.css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <title>Assistance Informatique à Dombasle-sur-Meurthe</title>
        <script type="text/javascript">
        	function calcul_annee(){
				var maDate = new Date(); 
				window.document.write(maDate.getFullYear());
			}
        </script>
        <script type="text/javascript" src="js/functionAddEvent.js"></script>
		<script type="text/javascript" src="js/contact.js"></script>
		<script type="text/javascript" src="js/xmlHttp.js"></script>
		<script type="text/javascript" src="js/css-pop.js"></script>
		
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
		  ga('create', 'UA-42824940-1', 'num1nfo.fr');
		  ga('send', 'pageview');
		</script>
    </head>
    
    <!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
    <!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
    <!--[if IE 8 ]><body class="ie8"><![endif]-->
    <!--[if IE 9 ]><body class="ie9"><![endif]-->
    <!--[if !IE]><!--><body><!--<![endif]-->
        <div id="bloc_page">
            <header>
                <div id="titre_principal">
                	<div id="haut_droite">
                    	<div id="secteur">
                			<a href="#"  onClick="popup('popUpDiv');">
								<img src="images/maps.png" alt="Secteur : Meurthe-et-Moselle"/><br>
								Secteur : Meurthe-et-Moselle (54)
							</a>             		
                		</div>
                		<div id="contact">
                    		<a href="mailto:contact@num1nfo.fr">
								<img src="images/contact.png" alt="Contactez num1nfo"/><br>
								contact@num1nfo.fr
							</a>              		
                		</div>
                		<div id="telephone">
	        				<a href="tel:0602508930">
	        					<img src="images/telephone.png" alt="Appellez num1nfo"/>
    	            			<br>
        	        			06 02 50 89 30
        	        		</a>
                		</div>
                	</div>
                	<div id="logo">
                    	<a href="index.php">
                    		<img src="images/num1nfo_logo.png" alt="Logo de num1nfo" style="border:none;"/>
                    	</a>
                    	<!--POPUP-->    
    					<div id="blanket" style="display:none;"></div>
						<div id="popUpDiv" style="display:none;">
    						<a href="#" onclick="popup('popUpDiv')" ><img src="images/croix.png" alt="Fermer" /></a>
    						<iframe class="mapsgoogle" src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=Rue+Albert+1er,+Dombasle-sur-Meurthe&amp;aq=4&amp;oq=rue+albert+&amp;sll=48.956128,6.274661&amp;sspn=2.193157,5.718384&amp;ie=UTF8&amp;hq=&amp;hnear=Rue+Albert+1er,+54110+Dombasle-sur-Meurthe,+Meurthe-et-Moselle,+Lorraine&amp;t=m&amp;ll=48.967597,5.765076&amp;spn=0.676161,1.374664&amp;z=10&amp;iwloc=A&amp;output=embed"></iframe>
						</div>	
						<!-- / POPUP--> 
                    </div>
                    <h2>Le Service Utile à Domicile</h2>
                </div>
            </header>
            <div id="offres">
            	<div class="offres_gauche">
            		<img src="offres/offres_gauche.png" alt="Images de l'offre du moment" />
            		<?php include('offres/offres_gauche.txt');?>
            	</div>
            	<div class="offres_droite">
            		<img src="offres/offres_droite.png" alt="Images News" />
            		<?php include('offres/offres_droite.txt');?>
            	</div>
            </div>
            <section>
				<div class="titre_article">
                	<h1>INITIATION ET COURS INFORMATIQUE</h1>
                </div>
				<div id="initiation">
					<div class="p"> 
						<img src="images/initiation.png" alt="Initiation"/>
						Vous apprendrez à vous servir d'<b>Internet</b>, envoyer des mails et maitriser les pièces jointes, <b>discuter avec vos enfants sur des messageries instantanées</b> (Skype, Facebook...) faire <b>vos achats</b> en toute <b>sécurité</b>, vendre des objets sur Ebay, LeBonCoin
						Vous saurez faire une chose primordiale: <b>protéger et conserver vos données personnelles</b> (photos, vidéo, données administratives, etc.)
						Vous apprendrez à vous servir de Microsoft Office (Word, Excel, Outlook, PowerPoint), à graver un CD/DVD, à gérer vos photos (Picasa).
					</div>
                	<div>
						<div class="table_prix_droite">
							<table class="table_prix">
								<tr class="tr_prix">
									<td class="td_prix_bas">Forfait 2h démarrer l'informatique</td>
									<td class="td_prix_droite"><div class="prix"><b>50 €</b></div></td>
									<td class="td_prix_bas"><b>Tarifs horaire dégressifs sur formation</b></td>
									<td class="td_prix_droite"><div class="prix_s">★</div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				
				<div class="titre_article">
                	<h1>CONSEILS ACHATS / ABONNEMENTS</h1>
                </div>
				<div id="conseil"> 
					<div class="p">
						<img src="images/conseil.png" alt="Conseils"/>
						Vous voulez acheter un <b>nouvel ordinateur</b>, une <b>nouvelle TV</b>, un <b>caméscope</b>, un <b>appareil photo</b> : je vous <b>conseillerai</b> sur le <b>meilleur rapport qualité/prix</b> et la meilleure adaptation à <b>vos besoins</b>
						Je vous <b>aiderai</b> pour avoir <b>l'abonnement télécom</b> le mieux <b>adapté</b> à votre budget et à votre consommation.
					</div>
					<div>
						<div class="table_prix_gauche">
							<table class="table_prix">
								<tr class="tr_prix">
									<td class="td_prix_bas"><b>Tarifs communiqués sur demande</b></td>
									<td class="td_prix_droite"><div class="prix_s">★</div></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="conseil_exemple">
						<div class="ce_milieu">
							<h2>Num1nfo vous accompagne dans vos démarches pour réduire de moitié vos dépenses mensuelles</h2>
						</div>
						<div class="ce_gauche">
							<div class="titre">
								<h2>VOS DEPENSES AVANT</h2>
							</div>
							
							<p>
								- abonnement téléphone fixe & internet : 50€ / mois<br>
								- abonnement par mobile : 30€ / mois<br>
								- abonnement cable et satelite : 40€ / mois
							</p>
							<div class="ab_prix">120€ / mois</div>
						</div>
						<div class="ce_droite">
							<div class="titre">
								<h2>VOS DEPENSES APRES</h2>
							</div>
							
							<p>
								- abonnement téléphone fixe & internet : 30€ / mois<br>
								- abonnement par mobile : 15€ / mois<br>
								- abonnement cable et satelite : 20€ / mois
							</p>
							<div class="ab_prix">65€ / mois</div>
						</div>
					</div>
                </div>
                
                <div class="titre_article">
                	<h1>INSTALLATION INFORMATIQUE</h1>
                </div>
				<div id="installation">
					<div class="p">
						<img src="images/installation.png" alt="Installation"/>
						Je vous <b>installerai</b> le nouvel ordinateur que vous venez d'acheter avec un <b>antivirus</b> et vous <b>connecterai sa box internet</b>, ainsi que <b>vos imprimantes, scanner, webcam, appareils photos, télé à internet</b>, etc.
						Votre ordinateur "rame" ou devient lent : je vous réinstalle tout et vous récupère, dans la mesure du possible, <b>vos données personnelles</b>.
					</div>
                	<div>
						<div class="table_prix_droite">
							<table class="table_prix">
								<tr class="tr_prix">
									<td class="td_prix_bas">Installation nouvel ordinateur</td>
									<td class="td_prix_droite"><div class="prix"><b>30 €</b></div></td>
									<td class="td_prix_bas">Installation Internet</td>
									<td class="td_prix_droite"><div class="prix"><b>30 €</b></div></td>
								</tr>
								<tr class="tr_prix">
									<td class="td_prix_bas">Forfait installation ordinateur + internet</td>
									<td class="td_prix_droite"><div class="prix"><b>50 €</b></div></td>
									<td class="td_prix_bas">Forfait installation ordinateur + internet +1H prise en main</td>
									<td class="td_prix_droite"><div class="prix"><b>75 €</b></div></td>
								</tr>
								<tr class="tr_prix">
									<td class="td_prix_bas">Installation Windows</td>
									<td class="td_prix_droite"><div class="prix"><b>70 €</b></div></td>
									<td class="td_prix_bas">Installation Windows et sauvegarde données personnelles</td>
									<td class="td_prix_droite"><div class="prix"><b>85 €</b></div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="titre_article">
                	<h1>NUMERISATION</h1>
                </div>
                <div id="numerisation">
					<div class="p">
						<img src="images/numerisation.png" alt="Numérisation"/>
						Vous voulez mettre sur votre ordinateur vos <b>anciens négatifs</b>, <b>anciennes photos</b> et <b>diapositives</b> : je vous le ferai
						Vos souvenirs de vacances sont restés sur des <b>cassettes</b> VHS ou de caméscope : appelez-moi et <b>je vous les mettrai sur DVD</b> pour les lire avec votre lecteur de salon.
					</div>
                	<div>
						<div class="table_prix_gauche">
							<table class="table_prix">
								<tr class="tr_prix">
									<td class="td_prix_bas">Numérisation cassettes avec montage basic (0 à 60mn)</td>
									<td class="td_prix_droite"><div class="prix"><b>50 €</b></div></td>
									<td class="td_prix_bas">Numérisation diapos, photos</td>
									<td class="td_prix_droite"><div class="prix"><b>0,30 €/unité</b></div></td>
								</tr>
								<tr class="tr_prix">
									<td class="td_prix_bas">par tranche de 10mn au-delà de 60mn</td>
									<td class="td_prix_droite"><div class="prix"><b>7 €</b></div></td>
									<td class="td_prix_bas">Numérisation K7 audio</td>
									<td class="td_prix_droite"><div class="prix"><b>6 €<br>/unité</b></div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				
                <div class="titre_article">
                	<h1>PLUS+</h1>
                </div>
                <div id="plus">
                	<div class="p">
                	</div>
                	<div>
                		<table class="table_prix">
							<tr class="tr_prix">
								<td class="td_prix_bas">Tarif horaire pour prestations non mentionnées ci-dessus</td>
								<td class="td_prix_droite"><div class="prix"><b>30 €</b></div></td>
								<td class="td_prix_bas"><b>Paiement en CESU accepté</b></td>
								<td class="td_prix_droite"><div class="prix_s">★</div></td>
							</tr>
						</table>
					</div>
				</div>
                <aside>
                    <h1>À propos de num1nfo</h1>
                    <img src="images/bulle.png" alt="" id="fleche_bulle" />
                    <div id="photo_aside"><img src="images/contact_num1nfo.png" alt="num1nfo" /></div>
                    <p><i>Je suis un passionné autodidacte en informatique et cela fait longtemps que trottait dans ma tête l'envie de créer une auto-entreprise qui répondrait à un besoin, que je pense conséquent, de service à la personne en informatique.</i></p>
					<p><i>Je me suis découvert le plaisir énorme de transmettre mes compétences acquises ces dernières années et je me suis rendu également compte que j'avais le sens du contact et donner un cours un soir de 18H à 20H ou un week-end n'est pas une contrainte mais un agréable moment.</i></p>        			
					<hr>
					<h1>Contacter num1nfo</h1>
					<div id="loadBar" style="display:none;">
						<strong>Chargement...</strong>
						<img src="images/loading.gif" alt="Chargement en cours..." title="Envoi de message" />
					</div>
					<div id="emailSuccess" style="display:none;">
						<strong style="color:white;background-color:green;">Votre message a correctement été envoyé</strong>
					</div>
					<div id="contactFormArea">
					<span style="text-align:left;">*Champs obligatires</span>
						<form action="scripts/contact.php" method="post" id="cForm">
							<fieldset>
								<input class="text" type="text" size="25" name="posName" id="posName" value="*NOM" onClick="this.value='';" onBlur="if (this.value=='') { this.value='*NOM'; }"/>
								<input class="text" type="text" size="25" name="posEmail" id="posEmail" value="ADRESSE ELECTRONIQUE" onClick="this.value='';" onBlur="if(this.value=='') { this.value='ADRESSE ELECTRONIQUE';} "/>
								<input class="text" type="text" size="25" name="posRegard" id="posRegard" value="SUJET" onClick="this.value='';" onBlur="if(this.value=='') { this.value='SUJET'; }"/>
								<textarea cols="50" rows="5" name="posText" id="posText" onClick="this.value='';" onBlur="if(this.value=='') { this.value='*ECRIRE A NUM1NFO...'; }">*ECRIRE A NUM1NFO...</textarea>
								<br>
								<label for="selfCC">
									<input type="checkbox" name="selfCC" id="selfCC" value="send" /><span style="text-align:left;">Recevoir ce message en copie</span>
								</label><br>
								<label>
									<input class="submit" type="submit" name="sendContactEmail" id="sendContactEmail" value="Envoyer" />
								</label>
							</fieldset>
						</form>
					</div>
					<br>
					<hr>
                   	<p>
                   		<a href="https://www.facebook.com/pages/Num1nfo/487509911316916" target="_blank"><img src="images/facebook.png" alt="Facebook" /></a>
                   		<a href="https://twitter.com/num1nfo" target="_blank"><img src="images/twitter.png" alt="Twitter" /></a>
                   		<a href="https://plus.google.com/101394056220832441124" rel="publisher" target="_blank"><img src="images/googleplus.png" alt="Google+" /></a>
                   	</p>
                	<iframe src="https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/pages/Num1nfo/487509911316916"
        				style="border:none; width:220px; height:100px">
        			</iframe>
        			<br>
        			<a href="https://twitter.com/num1nfo" class="twitter-follow-button" data-show-count="true" data-lang="fr">Suivre @num1nfo</a>
        			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>				
        			<br>
					<div class="g-plusone"></div>
					<!-- Placez cette ballise après la dernière balise Bouton +1. -->
					<script type="text/javascript">
						window.___gcfg = {lang: 'fr'};
		
  						(function() {
   							var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		   					po.src = 'https://apis.google.com/js/plusone.js';
    						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
 						})();
		 			</script>
                </aside>
            </section>
            
            <footer>             
				<div id="siret">
					<h1>SIRET</h1>
					<p>52459416500010</p>                                                           
				</div>
				<div id="tel">
					<h1>CONTACT</h1>
					<p><a href="tel:0602508930">06 02 50 89 30</a><br>
					<a href="mailto:contact@num1nfo.fr">contact@num1nfo.fr</a></p>
				</div>
				<div id="adresse">
					<h1>ADRESSE</h1>
					<p>Rue Albert 1er <br>
					54110 Dombasle-sur-Meurthe<br>
					<a href="http://goo.gl/maps/7cyKj" target="_blank">Cliquez-ici pour visualiser un plan</a></p>
				</div>
            </footer>
        </div>
        <div id="copyright">
	        <div class="validation_html5">
  		      	<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fnum1nfo.fr%2F;group=1" target="_blank">
					<img src="http://www.w3.org/html/logo/badge/html5-badge-v-semantics.png" width="38" height="106" alt="Technologies utilisées : HTML5 & CSS3 / Validation de la Sémantique par W3C" title="Technologies utilisées : HTML5 & CSS3 / Validation de la Sémantique par W3C">
				</a>
			</div>
        	<div class="texte">
        		<span class="num">num</span><span class="un">1</span><span class="nfo">nfo</span> &nbsp; &copy;
	        	Tous droits réservés <script type="text/javascript">calcul_annee();</script> - Création <a href="http://pancard.fr" target="_blank">pancard</a>
        	</div>
		</div>
    </body>
</html>
